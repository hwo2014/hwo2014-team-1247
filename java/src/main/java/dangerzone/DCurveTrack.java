package dangerzone;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

/**
 * Created by janne on 17.4.2014.
 */
public class DCurveTrack extends DTrackPiece {

  private Logger log = LoggerFactory.getLogger(getClass());

  private final double radius;
  private final double angle;
  private List<Integer> lanePositions;

  // FIXME: this needs to be adaptive
  //double friction = 0.15617f;
  //double friction = 0.1555d; // orig
  //double friction = 0.175d; // long curves 1
  //double friction = 0.195d;
  //double friction = 0.155d; // ?
  double friction = 0.155d;

  public DCurveTrack(double radius, double angle, List<Integer> lanePositions, boolean withSwitch){
    super(withSwitch);
    this.radius = radius;
    this.angle = angle;

    this.lanePositions = lanePositions;
  }

  //private static final double S_CURVE_MIDPOINT = 30d, S_CURVE_COMPRESSOR = 10;
  private static final double S_CURVE_MIDPOINT = 40d, S_CURVE_COMPRESSOR = 16.0d;

  protected static double getSpeedLimitMultiplier(double angle){
    //=1/(1+(POWER(Q$13;-(I12-30)/10)))
    return 1d/(1d+Math.pow(Math.E,-1d*(angle-S_CURVE_MIDPOINT)/S_CURVE_COMPRESSOR));
  }

  public double getAngle() {
    return angle;
  }

  public double getRadius() {
    return radius;
  }

  @Override
  public double getLength(int startTrack, int endTrack) {
    //log.trace("Getting index {} from lanepositions size {}", startTrack, lanePositions.size());
    int laneOffset = this.lanePositions.get(startTrack);
    // if this is a right turn, we need to multiply lane offsets by -1
    if (this.angle>0){
      laneOffset*=-1;
    }
    double actualRadius = this.radius + laneOffset;
    return (double) (2 * Math.PI * actualRadius * (Math.abs(this.angle) / 360));
  }

  public List<DCurveTrack> getAllCurveTracks(){
    List<DCurveTrack> remainingTracks = getRemainingCurveTracks();
    remainingTracks.add(this);
    DTrackPiece prev = getPrev();
    while (prev instanceof DCurveTrack && ((DCurveTrack)prev).getAngle() * this.angle > 0){
      remainingTracks.add((DCurveTrack)prev);
      prev = prev.getPrev();
    }
    return remainingTracks;
  }

  public List<DCurveTrack> getRemainingCurveTracks(){
    List<DCurveTrack> tracks = new ArrayList<>();
    tracks.add(this);
    DTrackPiece next = getNext();
    while (next instanceof DCurveTrack && ((DCurveTrack)next).getAngle() * this.angle > 0){
      tracks.add((DCurveTrack)next);
      next = next.getNext();
    }
    return tracks;
  }

  @Override
  public double getMaxSpeed() {
    // assume distance units are mm
    double radiusInMm = this.radius / 1000f;
    double speedMetersPerSec = (double) Math.sqrt(friction * 9.81 * radiusInMm);
    double mul = 1.0;

    double bigCurveAngle = this.angle;
    DTrackPiece prev = getPrev();
    while (prev instanceof DCurveTrack && ((DCurveTrack)prev).angle * this.angle > 0){
      //mul *= 0.90;
      bigCurveAngle+=((DCurveTrack) prev).angle;
      prev = prev.getPrev();
    }
    DTrackPiece next = getNext();
    while (next instanceof DCurveTrack && ((DCurveTrack)next).angle * this.angle > 0){
      bigCurveAngle+=((DCurveTrack) next).angle;
      next = next.getNext();
    }
    double speedMul = getSpeedLimitMultiplier(Math.abs(bigCurveAngle));
    return 1000 * (1-speedMul) + (speedMetersPerSec * 1000) * speedMul;
//    return speedMetersPerSec * 1000;// * mul; // millimeters per sec
  }

  public int getBestLane(){
    int shortestIndex = 0;
    double shortestRadius = Double.MAX_VALUE;

    for (int i = 0; i < this.lanePositions.size(); i++){
      int laneOffset = this.lanePositions.get(i);
      if (this.angle>0){
        laneOffset*=-1;
      }
      double laneRadius =  this.radius + laneOffset;
      if (laneRadius < shortestRadius){
        shortestRadius = laneRadius;
        shortestIndex = i;
      }
    }
    return shortestIndex;
  }


  @Override
  public String toString() {
    return "DCurveTrack{" +
        "radius=" + radius +
        ", angle=" + angle +
        ", lanePositions=" + lanePositions +
        ", friction=" + friction +
        '}';
  }
}
