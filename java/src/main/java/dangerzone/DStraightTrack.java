package dangerzone;

import dangerzone.msg.TrackPiece;

import java.util.List;

/**
 * Created by janne on 17.4.2014.
 */
public class DStraightTrack extends DTrackPiece{

  private final double length;

  private List<Integer> lanePositions;

  public DStraightTrack(double length, boolean withSwitch, List<Integer> lanePositions){
    super(withSwitch);
    this.length = length;

    this.lanePositions = lanePositions;
  }

  public double getLength() {
    return length;
  }

  private static final int SWITCH_TRACK_MARGIN = 4;

  @Override
  public double getLength(int startTrack, int endTrack) {
    if (startTrack == endTrack) {
      return this.length;
    }else{
      int trackDistance = Math.abs(lanePositions.get(startTrack) - lanePositions.get(endTrack));
      double distance = (double)(Math.sqrt( Math.pow(this.length - SWITCH_TRACK_MARGIN, 2) + Math.pow(trackDistance, 2) )) + SWITCH_TRACK_MARGIN;
      return distance;
    }

  }

  @Override
  public double getMaxSpeed() {
    return Double.MAX_VALUE;
  }


  @Override
  public String toString() {
    return "DStraightTrack{" +
        "length=" + length +
        ", lanePositions=" + lanePositions +
        "} " + super.toString();
  }
}
