package dangerzone;

/**
 * Created by janne on 17.4.2014.
 */
abstract class DTrackPiece {

  private DTrackPiece prev;
  private DTrackPiece next;

  private boolean withSwitch;

  public DTrackPiece(boolean withSwitch){
    this.withSwitch = withSwitch;
  }

  abstract double getLength(int startTrack, int endTrack);

  abstract double getMaxSpeed();

  public DTrackPiece getPrev() {
    return prev;
  }

  public void setPrev(DTrackPiece prev) {
    this.prev = prev;
  }

  public DTrackPiece getNext() {
    return next;
  }

  public void setNext(DTrackPiece next) {
    this.next = next;
  }

  public boolean isWithSwitch() {
    return withSwitch;
  }


  @Override
  public String toString() {
    return "DTrackPiece{" +
        "withSwitch=" + withSwitch +
        '}';
  }
}
