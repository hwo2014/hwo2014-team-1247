package dangerzone;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import dangerzone.msg.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.net.Socket;

/**
 * Created by Janne Kytömäki on 16.4.2014.
 */
public class DangerZoneBot {

  private static Logger log = LoggerFactory.getLogger(DangerZoneBot.class);

  private ObjectMapper objectMapper = new ObjectMapper();

  public static void main(String... args) throws IOException {
    String host = args[0];
    int port = Integer.parseInt(args[1]);
    String botName = args[2];
    String botKey = args[3];

    log.info("Connecting to {}:{} as {}/{}", host, port, botName, botKey);

    final Socket socket = new Socket(host, port);
    final PrintWriter writer = new PrintWriter(new OutputStreamWriter(socket.getOutputStream(), "utf-8"));

    final BufferedReader reader = new BufferedReader(new InputStreamReader(socket.getInputStream(), "utf-8"));

    new DangerZoneBot(reader, writer, new Join(botName, botKey));
  }

  private PrintWriter writer;

  public DangerZoneBot(final BufferedReader reader, final PrintWriter writer, final Join join) throws IOException {

    this.writer = writer;
    objectMapper.disable(SerializationFeature.INDENT_OUTPUT);

    String line;

    send(join);

    Game game = null;
    YourCar car = null;
    log.debug("DATA;tick;piece;speed;angle;pRadius;pAngle");
    while ((line = reader.readLine()) != null) {

      log.trace(line);

      Message m = null;

      try {
        m = objectMapper.readValue(line, Message.class);
      }catch(IOException e){
        log.error("Error de-serializing message: '{}'", line);
        continue;
      }

      if (m instanceof CarPositions){
        CarPositions carPositions = ((CarPositions)m);
        game.updateCarPositions(carPositions);

        log.debug("DATA;{};{};{};{};{};{}", carPositions.getGameTick(), game.getTrackPieceIndex(), game.getSpeed(), game.getAngle(), game.getCurrentTrackPieceRadius(), game.getCurrentTrackPieceAngle());
        String command = "";
        log.debug(line);
        log.trace("CarPositions: {}", m);
        log.debug("Index: {}, inPieceDistance: {}, speed: {}, angle: {}", game.getTrackPieceIndex(), game.getInPieceDistance(), game.getSpeed(), game.getAngle());
        Direction direction = game.shouldChangeLanes();
        if (direction !=Direction.NONE){
          if (direction == Direction.LEFT){
            log.trace("Switching lane to left");
            send(new SwitchLanes("Left", carPositions.getGameTick()));
          }else{
            log.trace("Switching lane to right");
            send(new SwitchLanes("Right", carPositions.getGameTick()));
          }
        }else {
          send(new Throttle(game.getThrottle(), carPositions.getGameTick()));
        }

      }else if(m instanceof Join){
        log.debug("Joined");
      }else if(m instanceof GameInit){
        log.debug("Race init: {}", line);
        GameInit gameInit = (GameInit)m;
        game = new Game(
            gameInit.getGameInitRace().getGameData().getTrack().getLanes(),
            gameInit.getGameInitRace().getGameData().getTrack().getPieces(),
            car
          );
        log.debug("Created Game");
      }else if (m instanceof GameEnd){
        log.debug("Race end: {}", m);
      }else if (m instanceof Crash){
        log.debug("Crash: {}", m);
      }else if (m instanceof GameStart) {
        log.debug("Race start: {}", m);
      }else if (m instanceof YourCar){
        car = (YourCar) m;
      }else if (m instanceof LapFinished){
        LapFinished lf = (LapFinished)m;
        log.info("\n\n *** Lap finished, lap no {} time {} ms *** \n\n", lf.getData().getLapTime().getLap(), lf.getData().getLapTime().getMillis());
      }else{
        log.warn("Unknown message: {}", m);
        send(new Ping());
      }

    }

    log.info("Finished.");
  }

  private void send(Message command){

    boolean debug = false;

    if (debug){
      try {
        objectMapper.writeValue(System.out, command);
      } catch (IOException e) {
        e.printStackTrace();
      }
    }else {
      try {
        String obj = objectMapper.writeValueAsString(command);
        log.trace("Sending: {}", obj);
        writer.println(obj);
        writer.flush();
      } catch (IOException e) {
        log.error("Error serializing command", e);
        e.printStackTrace();
      }
    }
  }

}
