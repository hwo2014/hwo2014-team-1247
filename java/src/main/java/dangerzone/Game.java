package dangerzone;

import dangerzone.msg.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by janne on 17.4.2014.
 */
public class Game{

  private Logger log = LoggerFactory.getLogger(getClass());

  private List<DTrackPiece> trackPieces = new ArrayList<>();

  private List<Integer> lanePositions;

  private String carName;

  private String carColor;


  double minSafeSpeed = Double.MAX_VALUE;

  public Game(List<Lane> lanes, List<TrackPiece> pieces, YourCar myCar){
    // Store the lane distances. Just in case sort them by index first.
    lanePositions = lanes.stream()
        .sorted(Comparator.comparing(Lane::getIndex))
        .map(Lane::getDistanceFromCenter)
        .collect(Collectors.toList());

    // Store the track pieces
    for (TrackPiece tp : pieces){
      DTrackPiece piece;
      if (tp.getAngle()!=0){
        piece = new DCurveTrack(tp.getRadius(), tp.getAngle(), lanePositions, tp.isWithSwitch());
      }else{
        piece = new DStraightTrack(tp.getLength(), tp.isWithSwitch(), lanePositions);
      }

      if (trackPieces.size()>0){
        piece.setPrev(trackPieces.get(trackPieces.size()-1));
        trackPieces.get(trackPieces.size()-1).setNext(piece);
      }
      trackPieces.add(piece);

    }

    if (trackPieces.size()>0){
      trackPieces.get(trackPieces.size()-1).setNext(trackPieces.get(0));
      trackPieces.get(0).setPrev(trackPieces.get(trackPieces.size()-1));
    }

    // get the lowest speed limit for the track
    for (DTrackPiece piece: trackPieces){
      if (piece.getMaxSpeed() < minSafeSpeed){
        minSafeSpeed = piece.getMaxSpeed();
      }
    }

    this.carName = myCar.getCarId().getName();
    this.carColor = myCar.getCarId().getColor();

  }

  List<List<CarPositionData>> carPositionHistory = new ArrayList<>();

  private CarPositionData currentPosition;
  private DTrackPiece currentTrackPiece;

  private int tick;

  public void updateCarPositions(CarPositions carPositions){
    tick = carPositions.getGameTick();
    List<CarPositionData> carPositionDataList = carPositions.getCarPositions();
    carPositionHistory.add(carPositionDataList);
    currentPosition = carPositionDataList.stream().filter(p -> p.getId().getName().equals(carName)).findAny().get();
    currentTrackPiece = trackPieces.get(currentPosition.getPiecePosition().getPieceIndex());

    // we're changing tracks on this track piece, so clear the variables
    if (currentTrackPiece == laneChangePlannedOn){
      laneChangePlannedOn = null;
      laneChangeDirection = Direction.NONE;
    }

  }

  private final int TICKS_PER_SEC = 60;

  public double getInPieceDistance(){
    return carPositionHistory.get(carPositionHistory.size()-1).stream().filter(p -> p.getId().getName().equals(carName)).findAny().get().getPiecePosition().getInPieceDistance();
  }

  public int getTrackPieceIndex(){
    return carPositionHistory.get(carPositionHistory.size()-1).stream().filter(p -> p.getId().getName().equals(carName)).findAny().get().getPiecePosition().getPieceIndex();
  }

  public double getAngle(){
    return carPositionHistory.get(carPositionHistory.size()-1).stream().filter(p -> p.getId().getName().equals(carName)).findAny().get().getAngle();
  }

  public int getLap(){
    return carPositionHistory.get(carPositionHistory.size()-1).stream().filter(p -> p.getId().getName().equals(carName)).findAny().get().getPiecePosition().getLap();
  }

  public double getTick(){
    return this.tick;
  }

  private double getAngularSpeed(){
    if (carPositionHistory.size()<2){
      return 0d;
    }
    double prevAngle = carPositionHistory.get(carPositionHistory.size()-2).stream().filter(p -> p.getId().getName().equals(carName)).findAny().get().getAngle();;
    double curAngle = getAngle();
    return curAngle - prevAngle;
  }

  public double getAccel(){
    double speed1 = getSpeed(1);
    double speed2 = getSpeed(0);
    return speed2-speed1;
  }

  public double getSpeed(){
    return getSpeed(0);
  }

  public double getSpeed(int n){
    if (carPositionHistory.size()<2+n){
      return 0;
    }
    CarPositionData prevPos = carPositionHistory.get(carPositionHistory.size()-2-n).stream().filter(p -> p.getId().getName().equals(carName)).findAny().get();
    CarPositionData curPos = carPositionHistory.get(carPositionHistory.size()-1-n).stream().filter(p -> p.getId().getName().equals(carName)).findAny().get();

    if (curPos.getPiecePosition().getPieceIndex() - prevPos.getPiecePosition().getPieceIndex() > 1){
      //TODO: Handling?
      throw new RuntimeException("Car skipped a track piece!");
    }

    DTrackPiece prevPiece = trackPieces.get(prevPos.getPiecePosition().getPieceIndex());
    DTrackPiece curPiece = trackPieces.get(curPos.getPiecePosition().getPieceIndex());

    // if track piece is the same for previous and current tick, we can just subtract the inPieceDistances to get speed / tick
    if (prevPiece == curPiece){
      return (curPos.getPiecePosition().getInPieceDistance() - prevPos.getPiecePosition().getInPieceDistance()) * TICKS_PER_SEC;
    }

    // no need to consider the endLaneIndex for track piece length - it doesn't matter for straight pieces
    // and curving pieces don't have lane switches
    double prevPieceLength = prevPiece.getLength(prevPos.getPiecePosition().getLane().getStartLaneIndex(), prevPos.getPiecePosition().getLane().getEndLaneIndex());
//    log.debug("prevPieceLength: {}", prevPieceLength);
    double speedPerTick = prevPieceLength - prevPos.getPiecePosition().getInPieceDistance() + curPos.getPiecePosition().getInPieceDistance();

    return speedPerTick * TICKS_PER_SEC;
  }

  public Direction shouldChangeLanes(){
    boolean switchEncountered = false;
    DTrackPiece firstSwitch = null;
    DTrackPiece dp = currentTrackPiece;

    // we have a pending lane change command
    if (laneChangePlannedOn != null){
      return Direction.NONE;
    }

    dp = dp.getNext();
    while(!switchEncountered){
      if (dp.isWithSwitch()){
        switchEncountered = true;
        firstSwitch = dp;
      }
      dp = dp.getNext();
    }
    int currentLane = currentPosition.getPiecePosition().getLane().getEndLaneIndex();

    double currentLaneLength = firstSwitch.getLength(currentLane, currentLane);
    double leftLaneLength = Double.MAX_VALUE;
    if (currentLane>0){
      leftLaneLength = firstSwitch.getLength(currentLane - 1, currentLane - 1);
    }
    double rightLaneLength = Double.MAX_VALUE;
    if (currentLane < lanePositions.size() - 1){
      rightLaneLength = firstSwitch.getLength(currentLane + 1, currentLane + 1);
    }
    while (!dp.isWithSwitch()){
      currentLaneLength += dp.getLength(currentLane, currentLane);
      if (currentLane>0){
        leftLaneLength += dp.getLength(currentLane - 1, currentLane - 1);
      }
      if (currentLane < lanePositions.size() - 1){
        rightLaneLength += dp.getLength(currentLane + 1, currentLane + 1);
      }
      dp = dp.getNext();
    }
    if (leftLaneLength < currentLaneLength){
      laneChangePlannedOn = firstSwitch;
      laneChangeDirection = Direction.LEFT;
    }else if (rightLaneLength < currentLaneLength){
      laneChangePlannedOn = firstSwitch;
      laneChangeDirection = Direction.RIGHT;
    }else{
      return Direction.NONE;
    }

    log.debug("Planning to change lane on track index {}", trackPieces.indexOf(laneChangePlannedOn));
    return laneChangeDirection;
    /*
    if (!switchEncountered && dp.isWithSwitch()){ // always switch ASAP?
      switchEncountered = true;
      firstSwitch = dp;
    }else if (switchEncountered && dp instanceof DCurveTrack){ // TODO: consider the case where the track piece is both a curve and a switch
      DCurveTrack ct = (DCurveTrack) dp;
      if (currentPosition.getPiecePosition().getLane().getEndLaneIndex() != ct.getBestLane() ) {
        laneChangeCommandGivenBecause = ct;
        laneChangePlannedOn = firstSwitch;
        if (ct.getBestLane() < currentPosition.getPiecePosition().getLane().getEndLaneIndex()){
          laneChangeDirection = Direction.LEFT;
        }else{
          laneChangeDirection = Direction.RIGHT;
        }
        log.debug("Planning to change lane on track index {}", trackPieces.indexOf(laneChangePlannedOn));
        return laneChangeDirection;
      }else{
        return Direction.NONE;
      }
    }*/

  }

  protected static double getBrakingDistance(double targetSpeed, double currentSpeed){

    if (currentSpeed <= targetSpeed){
      return 0d;
    }

    //FIXME: drag should adapt
    double drag = 1.212d;
    double targetSpeedToCurrentSpeedRatio = targetSpeed / currentSpeed;

    // seconds it will take to decelerate to target speed with 0.0 throttle
    double secondsToTargetSpeed = (double)(Math.log(1d/targetSpeedToCurrentSpeedRatio)/drag);

    // integrate the braking distance multiplier
    double forT0 = (double)((1d/drag) * Math.pow(Math.E, -1d*drag*0d));
    double forEnd = (double)((1d/drag) * Math.pow(Math.E, -1d*drag*secondsToTargetSpeed));

    // multiply by current speed to get the distance in game units
    return (forT0 - forEnd) * currentSpeed ;
  }

//  private DCurveTrack laneChangeCommandGivenBecause;
  private DTrackPiece laneChangePlannedOn;
  private Direction laneChangeDirection = Direction.NONE;

  private double brakeAngle = 0.0d;

  private DCurveTrack brakingFor, swingingFor;
  private boolean curveApexReached = false;
  private List<DCurveTrack> curveSlowedDown;



  public double getCurrentTrackPieceAngle(){
    if (currentTrackPiece instanceof DCurveTrack){
      return ((DCurveTrack) currentTrackPiece).getAngle();
    }
    return 0;
  }

  public double getCurrentTrackPieceRadius(){
    if (currentTrackPiece instanceof DCurveTrack){
      return ((DCurveTrack) currentTrackPiece).getRadius();
    }
    return 0;
  }

  private double throttle = 0.8;

  public double getThrottle(){

    if (true){
      return 0.8d;
    }
//    if (curPos.getPiecePosition().getPieceIndex()>2 && curPos.getPiecePosition().getInPieceDistance() > 55){
//      return 0.0f;
//    }

    // liear deaccel
//    int x = 65;
//    if (tick < x){
//      return throttle;
//    }
//    if (tick >= x) {
//      if (throttle>0.05) {
//        return throttle -= 0.05;
//      }else{
//        return 0;
//      }
//    }

    // dec sliding on straight
    /*
    if (tick >= 286) {
      return 0.0d;
    }*/

    // set class prop throttle to 0.8 before using!
    if (getAccel() > 3){
      return throttle;
    }

    double targetSpeed = 0;
    if (getLap() == 0){
      targetSpeed = 355;
    }else if (getLap() == 1){
      targetSpeed = 365;
    }else if (getLap() == 2){
      targetSpeed = 385;
    }
//    double diff = targetSpeed - getSpeed();


    if (getSpeed() > targetSpeed && throttle > 0){
      throttle-=0.01;
    }
    if (getSpeed() < targetSpeed && throttle < 1){
      throttle+=0.01;
    }
    if (true) {
      return throttle;
    }

    double angularSpeed = getAngularSpeed();

    if (currentTrackPiece instanceof DCurveTrack &&
        (((DCurveTrack)currentTrackPiece).getAllCurveTracks().contains(brakingFor) ||
        ((DCurveTrack)currentTrackPiece).getAllCurveTracks().contains(swingingFor))
        ){
      DCurveTrack curCurveTrack = (DCurveTrack) currentTrackPiece;
      if ( getAngle() * curCurveTrack.getAngle() > 0 && Math.abs(getAngle()) > 10d && angularSpeed * curCurveTrack.getAngle() < 0 ){ // angle of car is increasing towards the outer bank of the curve
        curveSlowedDown = ((DCurveTrack)currentTrackPiece).getAllCurveTracks();
      }else{
        if (curveSlowedDown == null || !curveSlowedDown.contains(currentTrackPiece)){
          swingingFor = curCurveTrack;
          log.debug("Swinging for {}", swingingFor);
          return 0.0f;
        }else{
          log.debug("Curve already slowed");
        }
      }
    }else{
      swingingFor = null;
    }
/*

    if (currentTrackPiece instanceof DCurveTrack){
      DCurveTrack curCurveTrack = (DCurveTrack) currentTrackPiece;
      if (brakeAngle == curCurveTrack.getAngle() || (curveApexReached && angularSpeed * curCurveTrack.getAngle()<0)){ // car's angle is "bouncing back"
        log.debug("Throttling after bounceback. BrakeAngle {}, angularSpeed {}, curve angle {}", brakeAngle, angularSpeed, curCurveTrack.getAngle());
        brakeAngle = curCurveTrack.getAngle();
        return 1.0f;
      }else if (brakingFor != null && brakingFor.getAngle() == curCurveTrack.getAngle()){
        if (Math.abs(getAngle())>10d){
          curveApexReached = true;
        }
        return 0.0f;
      }else{
        brakeAngle = 0.0d;
        curveApexReached = false;
      }
    }else{
      brakeAngle = 0.0d;
      curveApexReached = false;
    }*/


    /*if (getSpeed() < minSafeSpeed){
      return 1.0f;
    }*/

    int index = currentPosition.getPiecePosition().getPieceIndex();

    /*if (getSpeed() > currentTrackPiece.getMaxSpeed()){
      log.debug("Driving a curve, current speed {}, current track piece max speed {}", getSpeed(), currentTrackPiece.getMaxSpeed());
      return 0.0f;
    }*/

    double lookAheadDistance = getBrakingDistance(this.minSafeSpeed, getSpeed());
    log.debug("Lookahead distance is {}", lookAheadDistance);

    double distanceChecked =
        currentTrackPiece.getLength(currentPosition.getPiecePosition().getLane().getStartLaneIndex(), currentPosition.getPiecePosition().getLane().getEndLaneIndex()) -
            currentPosition.getPiecePosition().getInPieceDistance();

    int projectedLane = currentPosition.getPiecePosition().getLane().getEndLaneIndex();

    DTrackPiece dp = currentTrackPiece;
    while(distanceChecked < lookAheadDistance){
      dp = dp.getNext();
      index++;
      double brakingDistance = getBrakingDistance(dp.getMaxSpeed(), getSpeed());
      boolean skipPiece = false;
      if (dp instanceof DCurveTrack){
        List<DCurveTrack> currentCurvePieces = ((DCurveTrack)dp).getAllCurveTracks();
        if(currentCurvePieces.contains(currentTrackPiece) && currentCurvePieces.contains(swingingFor)){
          skipPiece = true;
        }
      }
      if ( !skipPiece && brakingDistance > distanceChecked){
        brakingFor = (DCurveTrack)dp; // assuming only curve track pieces have speed limits!
        log.debug("Braking distance {} from {} to {} exceeds distance {} to piece id:{} {}, commencing brake", brakingDistance, getSpeed(), dp.getMaxSpeed(), distanceChecked, index, dp);
        return 0.0f;
      }else{
        int exitLane = projectedLane;
        if (laneChangePlannedOn == dp){
          exitLane = ( laneChangeDirection == Direction.RIGHT ? exitLane + 1 : exitLane - 1 );
        }
        log.trace("dp: {}, projected lane: {}, exit lane: {}", dp, projectedLane, exitLane);
        distanceChecked += dp.getLength(projectedLane, exitLane);
        projectedLane = exitLane;
      }
    }

    return 1.0f;
  }
  /*


        CarPositionData pos = null;
        if (carName != null) {
          pos = ((CarPositions) m).getCarPositions().stream().filter(p -> p.getId().getName().equals(carName)).findAny().get();
        }
        log.debug("Throttle");
        send(new Throttle(0.5));
        if (pos != null && lastPosition != null){
          log.info("Speed is " +
              ((pos.getPiecePosition().getPieceIndex() -
                  lastPosition.getPiecePosition().getPieceIndex()) * 100 +
                  pos.getPiecePosition().getInPieceDistance() - lastPosition.getPiecePosition().getInPieceDistance()));
        }
        lastPosition = pos;
   */


}

enum Direction{
  NONE, LEFT, RIGHT;
}
