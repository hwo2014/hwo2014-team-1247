package dangerzone.msg;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by Janne Kytömäki on 16.4.2014.
 */
public class Car {

  @JsonProperty
  private CarId id;

  @JsonProperty
  private Dimensions dimensions;

  public CarId getId() {
    return id;
  }

  public void setId(CarId id) {
    this.id = id;
  }

  public Dimensions getDimensions() {
    return dimensions;
  }

  public void setDimensions(Dimensions dimensions) {
    this.dimensions = dimensions;
  }

  @Override
  public String toString() {
    return "Car{" +
            "id=" + id +
            ", dimensions=" + dimensions +
            '}';
  }
}
