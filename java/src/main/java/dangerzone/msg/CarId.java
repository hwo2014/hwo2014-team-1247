package dangerzone.msg;

/**
 * Created by Janne Kytömäki on 16.4.2014.
 */
public class CarId {

  private String name;

  private String color;

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getColor() {
    return color;
  }

  public void setColor(String color) {
    this.color = color;
  }

  @Override
  public String toString() {
    return "CarId{" +
            "name='" + name + '\'' +
            ", color='" + color + '\'' +
            '}';
  }
}
