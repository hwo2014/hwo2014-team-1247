package dangerzone.msg;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by Janne Kytömäki on 16.4.2014.
 */
public class CarPositionData {

  @JsonProperty
  private CarId id;

  @JsonProperty
  private double angle;

  @JsonProperty
  private PiecePosition piecePosition;

  public CarId getId() {
    return id;
  }

  public void setId(CarId id) {
    this.id = id;
  }

  public double getAngle() {
    return angle;
  }

  public void setAngle(double angle) {
    this.angle = angle;
  }

  public PiecePosition getPiecePosition() {
    return piecePosition;
  }

  public void setPiecePosition(PiecePosition piecePosition) {
    this.piecePosition = piecePosition;
  }

  @Override
  public String toString() {
    return "CarPositionData{" +
            "id=" + id +
            ", angle=" + angle +
            ", piecePosition=" + piecePosition +
            '}';
  }

}
