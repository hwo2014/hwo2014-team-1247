package dangerzone.msg;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

/**
 * Created by Janne Kytömäki on 16.4.2014.
 */
public class CarPositions extends ReceivedMessage{

  @JsonProperty("data")
  private List<CarPositionData> carPositions;

  public List<CarPositionData> getCarPositions() {
    return carPositions;
  }

  public void setCarPositions(List<CarPositionData> carPositions) {
    this.carPositions = carPositions;
  }

  @Override
  public String toString() {
    return "CarPositions{" +
            "carPositions=" + carPositions +
            "} " + super.toString();
  }
}
