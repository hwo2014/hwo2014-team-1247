package dangerzone.msg;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by janne on 21.4.2014.
 */
public class Command extends Message {

  @JsonProperty
  private int gameTick;

  public Command(int gameTick) {
    this.gameTick = gameTick;
  }

  public int getGameTick() {
    return gameTick;
  }

  public void setGameTick(int gameTick) {
    this.gameTick = gameTick;
  }


  @Override
  public String toString() {
    return "Command{" +
        "gameTick=" + gameTick +
        '}';
  }
}
