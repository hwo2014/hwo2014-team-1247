package dangerzone.msg;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by Janne Kytömäki on 16.4.2014.
 */
public class Crash extends ReceivedMessage {

  @JsonProperty("data")
  private CarId car;

  public CarId getCar() {
    return car;
  }

  public void setCar(CarId car) {
    this.car = car;
  }

  @Override
  public String toString() {
    return "Crash{" +
            "car=" + car +
            "} " + super.toString();
  }

}
