package dangerzone.msg;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by Janne Kytömäki on 16.4.2014.
 */
public class Dimensions {

  @JsonProperty
  private double length;

  @JsonProperty
  private double width;

  @JsonProperty
  private double guideFlagPosition;

  public double getLength() {
    return length;
  }

  public void setLength(double length) {
    this.length = length;
  }

  public double getWidth() {
    return width;
  }

  public void setWidth(double width) {
    this.width = width;
  }

  public double getGuideFlagPosition() {
    return guideFlagPosition;
  }

  public void setGuideFlagPosition(double guideFlagPosition) {
    this.guideFlagPosition = guideFlagPosition;
  }

  @Override
  public String toString() {
    return "Dimensions{" +
            "length=" + length +
            ", width=" + width +
            ", guideFlagPosition=" + guideFlagPosition +
            '}';
  }
}
