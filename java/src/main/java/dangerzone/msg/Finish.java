package dangerzone.msg;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by Janne Kytömäki on 16.4.2014.
 */
public class Finish extends ReceivedMessage{

  @JsonProperty("data")
  private CarId carId;

  public CarId getCarId() {
    return carId;
  }

  public void setCarId(CarId carId) {
    this.carId = carId;
  }

  @Override
  public String toString() {
    return "Finish{" +
            "carId=" + carId +
            "} " + super.toString();
  }
}
