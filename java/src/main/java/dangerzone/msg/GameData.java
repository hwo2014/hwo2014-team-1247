package dangerzone.msg;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

/**
 * Created by Janne Kytömäki on 16.4.2014.
 */
public class GameData {

  @JsonProperty
  private Track track;

  @JsonProperty
  private List<Car> cars;

  @JsonProperty
  private RaceSession raceSession;

  public Track getTrack() {
    return track;
  }

  public void setTrack(Track track) {
    this.track = track;
  }

  public List<Car> getCars() {
    return cars;
  }

  public void setCars(List<Car> cars) {
    this.cars = cars;
  }

  public RaceSession getRaceSession() {
    return raceSession;
  }

  public void setRaceSession(RaceSession raceSession) {
    this.raceSession = raceSession;
  }

  @Override
  public String toString() {
    return "GameData{" +
            "track=" + track +
            ", cars=" + cars +
            ", raceSession=" + raceSession +
            '}';
  }

}