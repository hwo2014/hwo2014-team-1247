package dangerzone.msg;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by Janne Kytömäki on 16.4.2014.
 */
public class GameEnd extends ReceivedMessage {

  @JsonProperty("data")
  private GameEndData gameEndData;

  public GameEndData getGameEndData() {
    return gameEndData;
  }

  public void setGameEndData(GameEndData gameEndData) {
    this.gameEndData = gameEndData;
  }

  @Override
  public String toString() {
    return "GameEnd{" +
            "gameEndData=" + gameEndData +
            "} " + super.toString();
  }

}
