package dangerzone.msg;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

/**
 * Created by Janne Kytömäki on 16.4.2014.
 */
public class GameEndData {

  @JsonProperty
  private List<GameEndRaceResult> results;

  @JsonProperty
  private List<GameEndLapResult> bestLaps;

  public List<GameEndRaceResult> getResults() {
    return results;
  }

  public void setResults(List<GameEndRaceResult> results) {
    this.results = results;
  }

  public List<GameEndLapResult> getBestLaps() {
    return bestLaps;
  }

  public void setBestLaps(List<GameEndLapResult> bestLaps) {
    this.bestLaps = bestLaps;
  }

  @Override
  public String toString() {
    return "GameEndData{" +
            "results=" + results +
            ", bestLaps=" + bestLaps +
            '}';
  }
}
