package dangerzone.msg;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by Janne Kytömäki on 16.4.2014.
 */
public class GameEndLapResult {

  @JsonProperty("car")
  private CarId carId;

  @JsonProperty
  private LapResult result;

  public CarId getCarId() {
    return carId;
  }

  public void setCarId(CarId carId) {
    this.carId = carId;
  }

  public LapResult getResult() {
    return result;
  }

  public void setResult(LapResult result) {
    this.result = result;
  }

  @Override
  public String toString() {
    return "GameEndLapResult{" +
            "carId=" + carId +
            ", result=" + result +
            '}';
  }
}
