package dangerzone.msg;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by Janne Kytömäki on 16.4.2014.
 */
public class GameEndRaceResult {

  @JsonProperty("car")
  private CarId carId;

  @JsonProperty
  private RaceResult result;

  public CarId getCarId() {
    return carId;
  }

  public void setCarId(CarId carId) {
    this.carId = carId;
  }

  public RaceResult getResult() {
    return result;
  }

  public void setResult(RaceResult result) {
    this.result = result;
  }

  @Override
  public String toString() {
    return "GameEndRaceResult{" +
            "carId=" + carId +
            ", result=" + result +
            '}';
  }
}
