package dangerzone.msg;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by Janne Kytömäki on 16.4.2014.
 */
public class GameInit extends ReceivedMessage{

  @JsonProperty("data")
  private GameInitRace gameInitRace;

  public GameInitRace getGameInitRace() {
    return gameInitRace;
  }

  public void setGameInitRace(GameInitRace gameInitRace) {
    this.gameInitRace = gameInitRace;
  }

  @Override
  public String toString() {
    return "GameInit{" +
            "gameInitRace=" + gameInitRace +
            "} " + super.toString();
  }
}
