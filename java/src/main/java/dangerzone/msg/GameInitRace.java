package dangerzone.msg;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by Janne Kytömäki on 16.4.2014.
 */
public class GameInitRace {

  @JsonProperty("race")
  private GameData gameData;

  public GameData getGameData() {
    return gameData;
  }

  public void setGameData(GameData gameData) {
    this.gameData = gameData;
  }

  @Override
  public String toString() {
    return "GameInitRace{" +
            "gameData=" + gameData +
            '}';
  }
}
