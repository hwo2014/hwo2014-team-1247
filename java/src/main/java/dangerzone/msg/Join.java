package dangerzone.msg;


import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by Janne Kytömäki on 16.4.2014.
 */
public class Join extends Message{

  @JsonProperty("data")
  private JoinData data;

  public Join(){

  }

  public Join(String name, String key) {
    data = new JoinData(name, key);
  }

  public JoinData getData() {
    return data;
  }

  public void setData(JoinData data) {
    this.data = data;
  }

  @Override
  public String toString() {
    return "Join{" +
        "data=" + data +
        '}';
  }
}
