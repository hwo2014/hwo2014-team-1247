package dangerzone.msg;


import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by Janne Kytömäki on 16.4.2014.
 */
public class JoinData{

  @JsonProperty
  private String name;

  @JsonProperty
  private String key;

  public JoinData(){

  }

  public JoinData(String name, String key) {
    this.name = name;
    this.key = key;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getKey() {
    return key;
  }

  public void setKey(String key) {
    this.key = key;
  }
}
