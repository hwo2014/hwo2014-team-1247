package dangerzone.msg;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by Janne Kytömäki on 16.4.2014.
 */
public class Lane {

  @JsonProperty
  private int distanceFromCenter;

  @JsonProperty
  private int index;

  public int getDistanceFromCenter() {
    return distanceFromCenter;
  }

  public void setDistanceFromCenter(int distanceFromCenter) {
    this.distanceFromCenter = distanceFromCenter;
  }

  public int getIndex() {
    return index;
  }

  public void setIndex(int index) {
    this.index = index;
  }

  @Override
  public String toString() {
    return "Lane{" +
            "distanceFromCenter=" + distanceFromCenter +
            ", index=" + index +
            '}';
  }
}
