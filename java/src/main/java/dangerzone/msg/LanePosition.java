package dangerzone.msg;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by Janne Kytömäki on 16.4.2014.
 */
public class LanePosition {

  @JsonProperty
  private int startLaneIndex;

  @JsonProperty
  private int endLaneIndex;

  public int getStartLaneIndex() {
    return startLaneIndex;
  }

  public void setStartLaneIndex(int startLaneIndex) {
    this.startLaneIndex = startLaneIndex;
  }

  public int getEndLaneIndex() {
    return endLaneIndex;
  }

  public void setEndLaneIndex(int endLaneIndex) {
    this.endLaneIndex = endLaneIndex;
  }

  @Override
  public String toString() {
    return "LanePosition{" +
            "startLaneIndex=" + startLaneIndex +
            ", endLaneIndex=" + endLaneIndex +
            '}';
  }
}
