package dangerzone.msg;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by Janne Kytömäki on 16.4.2014.
 */
public class LapFinished extends ReceivedMessage{

  @JsonProperty
  private LapFinishedData data;

  public LapFinishedData getData() {
    return data;
  }

  public void setData(LapFinishedData data) {
    this.data = data;
  }

  @Override
  public String toString() {
    return "LapFinished{" +
            "data=" + data +
            "} " + super.toString();
  }
}
