package dangerzone.msg;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by Janne Kytömäki on 16.4.2014.
 */
public class LapFinishedData {

  @JsonProperty
  private CarId car;

  @JsonProperty
  private LapResult lapTime;

  @JsonProperty
  private RaceResult raceTime;

  @JsonProperty
  private Ranking ranking;

  public CarId getCar() {
    return car;
  }

  public void setCar(CarId car) {
    this.car = car;
  }

  public LapResult getLapTime() {
    return lapTime;
  }

  public void setLapTime(LapResult lapTime) {
    this.lapTime = lapTime;
  }

  public RaceResult getRaceTime() {
    return raceTime;
  }

  public void setRaceTime(RaceResult raceTime) {
    this.raceTime = raceTime;
  }

  public Ranking getRanking() {
    return ranking;
  }

  public void setRanking(Ranking ranking) {
    this.ranking = ranking;
  }

  @Override
  public String toString() {
    return "LapFinishedData{" +
            "car=" + car +
            ", lapTime=" + lapTime +
            ", raceTime=" + raceTime +
            ", ranking=" + ranking +
            '}';
  }
}
