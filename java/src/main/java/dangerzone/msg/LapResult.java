package dangerzone.msg;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by Janne Kytömäki on 16.4.2014.
 */
public class LapResult extends Result {

  @JsonProperty
  private int lap;

  public int getLap() {
    return lap;
  }

  public void setLap(int lap) {
    this.lap = lap;
  }

  @Override
  public String toString() {
    return "LapResult{" +
            "lap=" + lap +
            "} " + super.toString();
  }
}
