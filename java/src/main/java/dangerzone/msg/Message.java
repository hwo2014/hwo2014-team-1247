package dangerzone.msg;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;

/**
 * Created by Janne Kytömäki on 16.4.2014.
 */
@JsonTypeInfo(
        use = JsonTypeInfo.Id.NAME,
        include = JsonTypeInfo.As.PROPERTY,
        property = "msgType"
        )
@JsonSubTypes({
        @JsonSubTypes.Type(value = CarPositions.class, name = "carPositions"),
        @JsonSubTypes.Type(value = Crash.class, name = "crash"),
        @JsonSubTypes.Type(value = GameInit.class, name = "gameInit"),
        @JsonSubTypes.Type(value = YourCar.class, name = "yourCar"),
        @JsonSubTypes.Type(value = GameStart.class, name = "gameStart"),
        @JsonSubTypes.Type(value = GameEnd.class, name = "gameEnd"),
        @JsonSubTypes.Type(value = LapFinished.class, name = "lapFinished"),
        @JsonSubTypes.Type(value = Finish.class, name = "finish"),
        @JsonSubTypes.Type(value = TournamentEnd.class, name = "tournamentEnd"),
        @JsonSubTypes.Type(value = Join.class, name = "join"),
        @JsonSubTypes.Type(value = Throttle.class, name = "throttle"),
        @JsonSubTypes.Type(value = Ping.class, name = "ping"),
        @JsonSubTypes.Type(value = SwitchLanes.class, name = "switchLane"),
  })
public class Message {

  public Message(){

  }

}
