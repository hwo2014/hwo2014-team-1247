package dangerzone.msg;

/**
 * Created by Janne Kytömäki on 16.4.2014.
 */
public class PointXY {

  private double x;

  private double y;

  public double getX() {
    return x;
  }

  public void setX(double x) {
    this.x = x;
  }

  public double getY() {
    return y;
  }

  public void setY(double y) {
    this.y = y;
  }

  @Override
  public String toString() {
    return "PointXY{" +
            "x=" + x +
            ", y=" + y +
            '}';
  }
}
