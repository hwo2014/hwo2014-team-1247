package dangerzone.msg;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by Janne Kytömäki on 16.4.2014.
 */
public class RaceResult extends Result {

  @JsonProperty
  private int laps;

  public int getLaps() {
    return laps;
  }

  public void setLaps(int laps) {
    this.laps = laps;
  }

  @Override
  public String toString() {
    return "RaceResult{" +
            "laps=" + laps +
            "} " + super.toString();
  }
}
