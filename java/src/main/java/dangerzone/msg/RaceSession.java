package dangerzone.msg;

/**
 * Created by Janne Kytömäki on 16.4.2014.
 */
public class RaceSession {

  private int laps;

  private long maxLapTimeMs;

  private boolean quickRace;

  public int getLaps() {
    return laps;
  }

  public void setLaps(int laps) {
    this.laps = laps;
  }

  public long getMaxLapTimeMs() {
    return maxLapTimeMs;
  }

  public void setMaxLapTimeMs(long maxLapTimeMs) {
    this.maxLapTimeMs = maxLapTimeMs;
  }

  public boolean isQuickRace() {
    return quickRace;
  }

  public void setQuickRace(boolean quickRace) {
    this.quickRace = quickRace;
  }

  @Override
  public String toString() {
    return "RaceSession{" +
            "quickRace=" + quickRace +
            ", maxLapTimeMs=" + maxLapTimeMs +
            ", laps=" + laps +
            '}';
  }
}
