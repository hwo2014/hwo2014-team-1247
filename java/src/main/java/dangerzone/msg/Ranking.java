package dangerzone.msg;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by Janne Kytömäki on 16.4.2014.
 */
public class Ranking {

  @JsonProperty
  private int overall;

  @JsonProperty
  private int fastestLap;

  public int getOverall() {
    return overall;
  }

  public void setOverall(int overall) {
    this.overall = overall;
  }

  public int getFastestLap() {
    return fastestLap;
  }

  public void setFastestLap(int fastestLap) {
    this.fastestLap = fastestLap;
  }

  @Override
  public String toString() {
    return "Ranking{" +
            "overall=" + overall +
            ", fastestLap=" + fastestLap +
            '}';
  }
}
