package dangerzone.msg;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by Janne Kytömäki on 16.4.2014.
 */
public class ReceivedMessage extends Message {

  @JsonProperty
  private String gameId;

  @JsonProperty
  private int gameTick;

  public String getGameId() {
    return gameId;
  }

  public void setGameId(String gameId) {
    this.gameId = gameId;
  }

  public int getGameTick() {
    return gameTick;
  }

  public void setGameTick(int gameTick) {
    this.gameTick = gameTick;
  }

  @Override
  public String toString() {
    return "ReceivedMessage{" +
            "gameId='" + gameId + '\'' +
            ", gameTick=" + gameTick +
            "} " + super.toString();
  }
}
