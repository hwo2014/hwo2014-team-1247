package dangerzone.msg;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by Janne Kytömäki on 16.4.2014.
 */
abstract class Result {

  @JsonProperty
  private int ticks;

  @JsonProperty
  private long millis;

  public int getTicks() {
    return ticks;
  }

  public void setTicks(int ticks) {
    this.ticks = ticks;
  }

  public long getMillis() {
    return millis;
  }

  public void setMillis(long millis) {
    this.millis = millis;
  }

  @Override
  public String toString() {
    return "Result{" +
            "ticks=" + ticks +
            ", millis=" + millis +
            '}';
  }
}
