package dangerzone.msg;

/**
 * Created by Janne Kytömäki on 16.4.2014.
 */
public class StartingPoint {

  private PointXY position;

  private double angle;

  public PointXY getPosition() {
    return position;
  }

  public void setPosition(PointXY position) {
    this.position = position;
  }

  public double getAngle() {
    return angle;
  }

  public void setAngle(double angle) {
    this.angle = angle;
  }

  @Override
  public String toString() {
    return "StartingPoint{" +
            "position=" + position +
            ", angle=" + angle +
            '}';
  }

}
