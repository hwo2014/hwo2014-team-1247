package dangerzone.msg;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by Janne Kytömäki on 16.4.2014.
 */
public class SwitchLanes extends Command {

  @JsonProperty("data")
  private String value;


  public SwitchLanes(String value, int gameTick) {
    super(gameTick);
    this.value = value;
  }

  public String getValue() {
    return value;
  }

  public void setValue(String value) {
    this.value = value;
  }

  @Override
  public String toString() {
    return "Throttle{" +
            "value=" + value +
            "} " + super.toString();
  }
}
