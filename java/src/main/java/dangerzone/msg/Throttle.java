package dangerzone.msg;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by Janne Kytömäki on 16.4.2014.
 */
public class Throttle extends Command {

  @JsonProperty("data")
  private double value;


  public Throttle(double value, int gameTick) {
    super(gameTick);
    this.value = value;

  }

  public double getValue() {
    return value;
  }

  public void setValue(double value) {
    this.value = value;
  }

  @Override
  public String toString() {
    return "Throttle{" +
            "value=" + value +
            "} " + super.toString();
  }

}
