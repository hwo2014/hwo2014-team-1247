package dangerzone.msg;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by Janne Kytömäki on 16.4.2014.
 */
public class TournamentEnd extends ReceivedMessage{

  @JsonProperty
  private Object data;

  public Object getData() {
    return data;
  }

  public void setData(Object data) {
    this.data = data;
  }

  @Override
  public String toString() {
    return "TournamentEnd{" +
            "data=" + data +
            "} " + super.toString();
  }
}
