package dangerzone.msg;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

/**
 * Created by Janne Kytömäki on 16.4.2014.
 */
public class Track {

  @JsonProperty
  private String id;

  @JsonProperty
  private String name;

  @JsonProperty
  private List<TrackPiece> pieces;

  @JsonProperty
  private List<Lane> lanes;

  @JsonProperty
  private StartingPoint startingPoint;

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public List<TrackPiece> getPieces() {
    return pieces;
  }

  public void setPieces(List<TrackPiece> pieces) {
    this.pieces = pieces;
  }

  public List<Lane> getLanes() {
    return lanes;
  }

  public void setLanes(List<Lane> lanes) {
    this.lanes = lanes;
  }

  public StartingPoint getStartingPoint() {
    return startingPoint;
  }

  public void setStartingPoint(StartingPoint startingPoint) {
    this.startingPoint = startingPoint;
  }

  @Override
  public String toString() {
    return "Track{" +
            "id='" + id + '\'' +
            ", name='" + name + '\'' +
            ", pieces=" + pieces +
            ", lanes=" + lanes +
            ", startingPoint=" + startingPoint +
            '}';
  }
}
