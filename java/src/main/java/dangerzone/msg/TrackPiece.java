package dangerzone.msg;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by Janne Kytömäki on 16.4.2014.
 */
public class TrackPiece {

  @JsonProperty
  private double length;

  @JsonProperty("switch")
  private boolean withSwitch;

  @JsonProperty
  private double radius;

  @JsonProperty
  private double angle;

  public double getLength() {
    return length;
  }

  public void setLength(double length) {
    this.length = length;
  }

  public double getRadius() {
    return radius;
  }

  public void setRadius(double radius) {
    this.radius = radius;
  }

  public double getAngle() {
    return angle;
  }

  public void setAngle(double angle) {
    this.angle = angle;
  }

  public boolean isWithSwitch() {
    return withSwitch;
  }

  public void setWithSwitch(boolean withSwitch) {
    this.withSwitch = withSwitch;
  }

  @Override
  public String toString() {
    return "TrackPiece{" +
            "length=" + length +
            ", withSwitch=" + withSwitch +
            ", radius=" + radius +
            ", angle=" + angle +
            '}';
  }
}
