package dangerzone;

import junit.framework.TestCase;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Arrays;

import static junit.framework.Assert.assertEquals;

/**
 * Created by janne on 18.4.2014.
 */
public class DCurveTrackTest {

  private Logger log = LoggerFactory.getLogger(getClass());

  DCurveTrack track;
  @Before
  public void before(){
    track = new DCurveTrack(100, 45.0f, Arrays.asList(new Integer[]{-10, 10}), false);
  }
  @Test
  public void lengthTest(){
    assertEquals(8639, Math.round((track.getLength(0,0) * 100)));
  }

  @Test
  public void maxSpeedTest(){
    log.debug("{}", track.getMaxSpeed());
  }

  @Test
  public void speedMulTest(){
    log.debug("{}", DCurveTrack.getSpeedLimitMultiplier(45d));
  }

}

