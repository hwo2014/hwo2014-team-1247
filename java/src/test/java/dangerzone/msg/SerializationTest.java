package dangerzone.msg;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;

/**
 * Created by Janne Kytömäki on 16.4.2014.
 */
public class SerializationTest {

  private final static String
      CAR_POSITIONS_MSG =
      "{\"msgType\":\"carPositions\",\"data\":[{\"id\":{\"name\":\"KennyLoggins\",\"color\":\"red\"}," +
          "\"angle\":0.0,\"piecePosition\":{\"pieceIndex\":3,\"inPieceDistance\":99.32442504102038," +
          "\"lane\":{\"startLaneIndex\":0,\"endLaneIndex\":0},\"lap\":0}}]," +
          "\"gameId\":\"04b84715-1899-4458-b610-573636787d32\",\"gameTick\":80}",
      CRASH_MSG =
          "{\"msgType\":\"crash\",\"data\":{\"name\":\"KennyLoggins\",\"color\":\"red\"}," +
              "\"gameId\":\"04b84715-1899-4458-b610-573636787d32\",\"gameTick\":96}",
      GAME_INIT_MSG =
          "{\"msgType\":\"gameInit\",\"data\":{\"race\":{\"track\":{\"id\":\"keimola\",\"name\":\"Keimola\"," +
              "\"pieces\":[{\"length\":100.0},{\"length\":100.0},{\"length\":100.0},{\"length\":100.0," +
              "\"switch\":true},{\"radius\":100,\"angle\":45.0},{\"radius\":100,\"angle\":45.0}," +
              "{\"radius\":100,\"angle\":45.0},{\"radius\":100,\"angle\":45.0},{\"radius\":200," +
              "\"angle\":22.5,\"switch\":true},{\"length\":100.0},{\"length\":100.0},{\"radius\":200," +
              "\"angle\":-22.5},{\"length\":100.0},{\"length\":100.0,\"switch\":true},{\"radius\":100," +
              "\"angle\":-45.0},{\"radius\":100,\"angle\":-45.0},{\"radius\":100,\"angle\":-45.0}," +
              "{\"radius\":100,\"angle\":-45.0},{\"length\":100.0,\"switch\":true},{\"radius\":100," +
              "\"angle\":45.0},{\"radius\":100,\"angle\":45.0},{\"radius\":100,\"angle\":45.0},{\"radius\":100," +
              "\"angle\":45.0},{\"radius\":200,\"angle\":22.5},{\"radius\":200,\"angle\":-22.5}," +
              "{\"length\":100.0,\"switch\":true},{\"radius\":100,\"angle\":45.0},{\"radius\":100," +
              "\"angle\":45.0},{\"length\":62.0},{\"radius\":100,\"angle\":-45.0,\"switch\":true}," +
              "{\"radius\":100,\"angle\":-45.0},{\"radius\":100,\"angle\":45.0},{\"radius\":100,\"angle\":45.0}," +
              "{\"radius\":100,\"angle\":45.0},{\"radius\":100,\"angle\":45.0},{\"length\":100.0," +
              "\"switch\":true},{\"length\":100.0},{\"length\":100.0},{\"length\":100.0},{\"length\":90.0}]," +
              "\"lanes\":[{\"distanceFromCenter\":-10,\"index\":0},{\"distanceFromCenter\":10,\"index\":1}]," +
              "\"startingPoint\":{\"position\":{\"x\":-300.0,\"y\":-44.0},\"angle\":90.0}}," +
              "\"cars\":[{\"id\":{\"name\":\"KennyLoggins\",\"color\":\"red\"},\"dimensions\":{\"length\":40.0," +
              "\"width\":20.0,\"guideFlagPosition\":10.0}}],\"raceSession\":{\"laps\":3,\"maxLapTimeMs\":60000," +
              "\"quickRace\":true}}},\"gameId\":\"04b84715-1899-4458-b610-573636787d32\"}",
      GAME_END_MSG =
          "{\"msgType\":\"gameEnd\"," +
              "\"data\":{\"results\":[{\"car\":{\"name\":\"KennyLoggins\",\"color\":\"red\"},\"result\":{\"laps\":3," +
              "\"ticks\":2149,\"millis\":35817}}],\"bestLaps\":[{\"car\":{\"name\":\"KennyLoggins\",\"color\":\"red\"}," +
              "\"result\":{\"lap\":1,\"ticks\":700,\"millis\":11666}}]},\"gameId\":\"decfa7d9-b007-43fb-a758-a77d5b58d4ae\"}",
      LAP_FINISHED_MSG =
          "{\"msgType\":\"lapFinished\",\"data\":{\"car\":{\"name\":\"KennyLoggins\"," +
              "\"color\":\"red\"},\"lapTime\":{\"lap\":2,\"ticks\":700,\"millis\":11667},\"raceTime\":{\"laps\":3," +
              "\"ticks\":2150,\"millis\":35833},\"ranking\":{\"overall\":1,\"fastestLap\":1}}," +
              "\"gameId\":\"decfa7d9-b007-43fb-a758-a77d5b58d4ae\"}",
      FINISH_MSG =
          "{\"msgType\":\"finish\",\"data\":{\"name\":\"KennyLoggins\",\"color\":\"red\"}," +
              "\"gameId\":\"decfa7d9-b007-43fb-a758-a77d5b58d4ae\"}",
      TOURNAMENT_END_MSG =
          "{\"msgType\":\"tournamentEnd\",\"data\":null," +
              "\"gameId\":\"decfa7d9-b007-43fb-a758-a77d5b58d4ae\"}",
      JOIN_MSG =
          "{\"msgType\":\"join\",\"data\":{\"name\":\"KennyLoggins\",\"key\":\"+LwNcHCfyfkumw\"}}";


  private ObjectMapper objectMapper = new ObjectMapper();

  @Before
  public void setup() {
  }

  @Test
  public void carPositionsTest() throws IOException {

    Message m = objectMapper.readValue(CAR_POSITIONS_MSG, Message.class);

    System.out.println(m);

  }


  @Test
  public void crashTest() throws IOException {

    Message m = objectMapper.readValue(CRASH_MSG, Message.class);

    System.out.println(m);

  }

  @Test
  public void gameInitTest() throws IOException {
    Message m = objectMapper.readValue(GAME_INIT_MSG, Message.class);
    System.out.println(m);
  }

  @Test
  public void gameInitTimedTest() throws IOException {
    double n = 10000;
    long start = System.currentTimeMillis();
    for (int i = 0; i < n; i++) {
      Message m = objectMapper.readValue(GAME_INIT_MSG, Message.class);
    }
    long end = System.currentTimeMillis();
    double timePerSerialization = (double) ((end - start) / n);
    System.out.println(timePerSerialization + "ms");

  }


  @Test
  public void gameEndTest() throws IOException {
    Message m = objectMapper.readValue(GAME_END_MSG, Message.class);
    System.out.println(m);
  }


  @Test
  public void lapFinishedTest() throws IOException {
    Message m = objectMapper.readValue(LAP_FINISHED_MSG, Message.class);
    System.out.println(m);
  }

  @Test
  public void finishTest() throws IOException {
    Message m = objectMapper.readValue(FINISH_MSG, Message.class);
    System.out.println(m);
  }

  @Test
  public void tournamentEndTest() throws IOException {
    Message m = objectMapper.readValue(TOURNAMENT_END_MSG, Message.class);
    System.out.println(m);
  }

  @Test
  public void joinTest() throws IOException {
    Message m = objectMapper.readValue(JOIN_MSG, Message.class);
    System.out.println(m);
  }

  @Test
  public void deserializeThrottleTest() throws JsonProcessingException {
    Throttle t = new Throttle(1.0d, 123);
    System.out.println(objectMapper.writeValueAsString(t));
  }

}
